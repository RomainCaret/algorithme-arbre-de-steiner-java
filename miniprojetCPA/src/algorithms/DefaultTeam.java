package algorithms;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

public class DefaultTeam {
	private double[][] dist;
	public static final int BUDGET =1664;
	public Tree2D calculSteiner(ArrayList<Point> points, int edgeThreshold, ArrayList<Point> hitPoints) {
		boolean cherche;//variable pour savoir si le triangle vaut le coup qu'on cherche son barycentre ou non
		int[][] paths;//tous les paths menant d'un point [i][j] vers un autre point [i][j]
		paths = calculShortestPaths(points, edgeThreshold); //calcul de paths

		ArrayList<Point> list_bary = new ArrayList<Point>(); //list des barycentres à évaluer à la fin
		ArrayList<Point> list_bary_opti = new ArrayList<Point>(); //list des barycentres à évaluer à la fin

		Tree2D arbre_couvrant; //arbre couvrant temporaire qu'on va calculer et évaluer la distance
		double d_tmp; //distance de l'arbre couvrant temporaire
		double d_min; //distance de l'arbre couvrant minimal

		//Arbre de réference pour comparer ensuite avec les nouveaux
		//Arbre sans barycentre
		arbre_couvrant = calculSteiner_simple(points, paths, hitPoints); //calcul simple de l'arbre couvrant sans barycentre
		d_min = arbre_couvrant.distanceTrees(); // calcul de la distance de l'arbre couvrant
		System.out.println(d_min);
		// On évalue tous les triangles possibles à partir des hitPoints pour voir si ajouter un point
		//barycentre du triangle pourrait faire diminuer la distance totale
		for (int k = 0; k < hitPoints.size(); k++) {
			for (int i = k + 1; i < hitPoints.size(); i++) {
				for (int j = i + 1; j < hitPoints.size(); j++) {
					cherche = true;
					for (Point iterPoint :  hitPoints){
						if ( triangleContientPoint(hitPoints.get(i), hitPoints.get(j), hitPoints.get(k), iterPoint)){
						// On ne cherche pas de barycentre si le triangle contient d'autres hitpoints	
							cherche = false;
							break;
						}
					}
					if (cherche)
						list_bary.add(getBarycentre_ameliore(i, j, k, hitPoints, points));
						//si le barycentre vaut le coup, il est calculé puis ajouté à la liste des barycentres à évaluer
				}
			}
		}
		for(Point iter_point: list_bary) {
			hitPoints.add(iter_point);
			arbre_couvrant = calculSteiner_simple(points, paths, hitPoints); // on calcule apres avoir ajouté le barycentre le nouvel arbre couvrant
			d_tmp = arbre_couvrant.distanceTrees(); // on calcule la distance de ce nouvel arbre
			if (d_tmp < d_min) {
				//System.out.println("optimisation : " +d_tmp +"Point : "+iter_point);
				//Recherche du minimum arbre couvrant parmis les barycentres
				//On met à jour la distance minimale
				d_min = d_tmp;
				list_bary_opti.add(iter_point);
			}else
				hitPoints.remove(iter_point);
				//si la distance est moins bonne que l'ancien arbre alors on enleve le barycentre qui vient d'etre amené
		}
		for(Point iter_point: list_bary_opti) {
			if (hitPoints.contains(iter_point)){
				hitPoints.remove(iter_point);
				arbre_couvrant = calculSteiner_simple(points, paths, hitPoints); // on calcule apres avoir ajouté le barycentre le nouvel arbre couvrant
				d_tmp = arbre_couvrant.distanceTrees(); // on calcule la distance de ce nouvel arbre
				if (d_tmp <= d_min) {
					//System.out.println("optimisation : " +d_tmp +"Point : "+iter_point);
					//Recherche du minimum arbre couvrant parmis les barycentres
					//On met à jour la distance minimale
					d_min = d_tmp;
				}else
					hitPoints.add(iter_point);
					//si la distance est moins bonne que l'ancien arbre alors on enleve le barycentre qui vient d'etre amené
			}
			
		}
		//On recalcule l'arbre minimal qui n'a pas ete conservé en mémoire dans une variable arbre_couvrant_min
		return calculSteiner_simple(points, paths, hitPoints);
	}

	private boolean triangleContientPoint(Point a, Point b, Point c, Point x) {
		//Renvoie true si le Point x est dans le triangle a,b,c
		double l1 = ((b.y - c.y) * (x.x - c.x) + (c.x - b.x) * (x.y - c.y))
				/ (double) ((b.y - c.y) * (a.x - c.x) + (c.x - b.x) * (a.y - c.y));
		double l2 = ((c.y - a.y) * (x.x - c.x) + (a.x - c.x) * (x.y - c.y))
				/ (double) ((b.y - c.y) * (a.x - c.x) + (c.x - b.x) * (a.y - c.y));
		double l3 = 1 - l1 - l2;
		return (0 < l1 && l1 < 1 && 0 < l2 && l2 < 1 && 0 < l3 && l3 < 1);
	}

	public Point getBarycentre(int i, int j, int k, ArrayList<Point> hitPoints, ArrayList<Point> points) {
		double barycentrex = (hitPoints.get(i).x + hitPoints.get(k).x + hitPoints.get(j).x) / 3;
		double barycentrey = (hitPoints.get(i).x + hitPoints.get(k).x + hitPoints.get(j).x) / 3;

		double min = Double.MAX_VALUE;
		int i_min = 0;
		double dist;
		//Version du barycentre qui cherche le barycentre géométrique puis regarde le point
		//dont la distance à "vol d'oiseau" est la plus petite
		//Donc pas optimal si ce point est esseulé et on doit faire un détour pour aller le chercher
		//Ne tient pas en compte de la configuration des points
		for (int index = 0; index < points.size(); index++) {
			dist = (barycentrex - points.get(index).x) * (barycentrex - points.get(index).x)
					+ (barycentrey - points.get(index).y) * (barycentrey - points.get(index).y);
			if (min > dist) {
				// On  recupere le minimum
				min = dist;
				i_min = index;
			}
		}
		return points.get(i_min);
	}

	public Point getBarycentre_ameliore(int i, int j, int k, ArrayList<Point> hitPoints, ArrayList<Point> points) {
		double min = Double.MAX_VALUE;
		int i_min = 0;
		double dist_tmp;

		//Ici le barycentre est calculé en cherchant le point qui minimise les distances entre les points du triangle
		//En fait ici on cherche l'arbre couvrant minimal du triangle i, j, k mais en utilisant la liste des points disponibles
		for (int index = 0; index < points.size(); index++) {
			//L'utilisation d'indexOf n'est pas optimal car il se fait en O(n) et n'est donc pas optimal
			//Si on veut améliorer la vitesse de l'algo,
			//il faudrait creer une hashmap qui serait mieux pour recuperer les index
			dist_tmp = dist[index][points.indexOf(hitPoints.get(i))]
					+dist[index][points.indexOf(hitPoints.get(j))] 
							+dist[index][points.indexOf(hitPoints.get(k))];
			if (min > dist_tmp) {
				// On  recupere le minimum
				min = dist_tmp;
				i_min = index;
			}
		}
		return points.get(i_min);
	}
	private ArrayList<Tree2D> chemin;
	public int calculSteinerBudgetRec(int parcouru,int nbPoints,Tree2D self, Tree2D from, ArrayList<Point> points, ArrayList<Point> hitPoints) {
		
		int max = nbPoints;
		System.out.println("parcourur:"+parcouru);
		Tree2D maxTree = null;
		int val;
		double val2;
		for(Tree2D iter : self.getSubTrees()){
			if (iter == from)
				continue;
			val2 = dist[points.indexOf(self.getRoot())][points.indexOf(iter.getRoot())];
			if (parcouru+val2 <= BUDGET){
				if (hitPoints.contains(iter.getRoot()))
					val = calculSteinerBudgetRec((int)(parcouru+val2), nbPoints+1, iter, self, points, hitPoints);
				else
					val = calculSteinerBudgetRec((int)(parcouru+val2), nbPoints, iter, self, points, hitPoints);				
				if (val > max){
					max = val;
					maxTree = iter;
				}
			}
		}
		if (maxTree != null)
			chemin.add(maxTree);
		return max;
	}

	public Tree2D geneTree2d(int index){
		int fils_index = index;
		if (index < 0)
			return null;
		
		Tree2D arbre;
		Tree2D arbre_fils;
		

		ArrayList<Tree2D> trees = new ArrayList<Tree2D>();
		ArrayList<Tree2D> trees2 = chemin.get(index).getSubTrees();
		fils_index--;
		while (fils_index >= 0 && trees2.contains(chemin.get(fils_index)) == false) {
				fils_index--;
		}
		System.out.println("fi : "+fils_index);
		arbre_fils = geneTree2d(fils_index);
		if (arbre_fils != null)
			trees.add(arbre_fils);
		arbre = new Tree2D(chemin.get(index).getRoot(), trees);
		return arbre;
	}

	//idée on parcourt tant qu'on rencontre pas de hitpoints, des qu'on est sur hitpoint mesure la distance parcourue
	//plus de parcourt possible, on met dans l'arbre la distance minimal au hitpoints, on reparcourt etc...
	//s'arrete quand le budget est depassé
	//(pas optimal si une grande distance cache une petite derriere)
	public ArrayList<Tree2D> creation_ajout(Tree2D elem_from,Tree2D elem_to, ArrayList<Tree2D> tab){
		ArrayList<Tree2D> ret;
		if (tab != null) {
			ret =(ArrayList<Tree2D>)tab.clone();
			ret.remove(ret.size()-1);
			ret.remove(ret.size()-1);
		}else
			ret = new ArrayList<Tree2D>();
		ret.add(elem_from);
		ret.add(elem_to);
		return ret;
	}
	public void calculSteinerBudgetRec2(ArrayList<Tree2D> parcour_liste_avant, ArrayList<ArrayList<Tree2D>> parcour_liste_from, ArrayList<Double> parcour_liste_dist ,ArrayList<Boolean> parcour_liste_arrive, double parcouru, ArrayList<Point> points, ArrayList<Point> hitPoints, int cpt_bug){
		double distance;
		boolean new_embranchement;
		cpt_bug++;
		Tree2D iter_from;
		if (parcour_liste_from.size() == 0)
			return;
		//System.out.println("Parcouru : " + parcouru);
		//System.out.println(parcour_liste_arrive);
		ArrayList<Tree2D> parcour_liste_remove = new ArrayList<Tree2D>();
		ArrayList<Tree2D> parcour_liste_suivant = new ArrayList<Tree2D>();

		
		if (parcour_liste_arrive.contains(false) == false ||parcour_liste_avant.size()==0){
			Double min = Double.MAX_VALUE;
			int i_min = -1;
			int cpt = 0;
			//System.out.println("fin");
			for (Double dist_to_hitpoint : parcour_liste_dist){
				if (dist_to_hitpoint < min){
					i_min = cpt;
					min = dist_to_hitpoint;
				}
				cpt++;
			}
			if (i_min == -1 || parcouru + min > BUDGET) { //Cas de base
				//System.out.println(i_min +"VRAI FIN"+parcouru);
				return ;
			
			}//Branchement vers Tree sur imin
			parcouru += min;
			//System.out.println("parcouru : " +parcouru);
			ArrayList<Tree2D> chemin_win = parcour_liste_from.get(i_min);
			for (int i = 0; i < chemin_win.size();i = i+2){
				chemin_win.get(i).getSubTrees().add(chemin_win.get(i+1));
			}
			parcour_liste_from.set(i_min, new ArrayList<Tree2D>());
			parcour_liste_arrive.set(i_min, false);
			parcour_liste_dist.set(i_min, Double.valueOf(0));
			calculSteinerBudgetRec2(parcour_liste_avant, parcour_liste_from, parcour_liste_dist, parcour_liste_arrive, parcouru, points, hitPoints, cpt_bug);
			
			return ;
		}
		parcour_liste_suivant = (ArrayList<Tree2D>) parcour_liste_avant.clone();
		//System.out.println("\n\nTOUR : "+cpt_bug);
		
		for(int index = 0;index < parcour_liste_avant.size(); index++){
			//System.out.println("Pere (avant, arrive, dist, from)");
			//System.out.println(parcour_liste_avant.get(index));
			//System.out.println(parcour_liste_arrive.get(index));
			//System.out.println(parcour_liste_dist.get(index));
			//System.out.println(parcour_liste_from.get(index));
			//System.out.println("Fin pere");
			if (parcour_liste_arrive.get(index) == true)
				continue;
			new_embranchement = false;
			iter_from = parcour_liste_avant.get(index);
			//System.out.println("Taille fils " + iter_from.getSubTrees().size());
			for(Tree2D iter_to : iter_from.getSubTrees()){
				
				if (iter_from.getRoot() == iter_to.getRoot()){
					parcour_liste_remove.add(iter_to);
					continue;
				}
				//System.out.println("Fils"+ iter_to);
				distance = dist[points.indexOf(iter_from.getRoot())][points.indexOf(iter_to.getRoot())];
				if (new_embranchement == false && parcour_liste_dist.get(index) == 0) {
					parcour_liste_remove.add(iter_to);
					parcour_liste_from.set(index,creation_ajout(iter_from,iter_to, null));
				}
				if (new_embranchement == true && hitPoints.contains(iter_to.getRoot())){
					//System.out.println("true et pomme");
					parcour_liste_remove.add(iter_to);
					parcour_liste_arrive.add(true);
					parcour_liste_from.add(creation_ajout(iter_from,iter_to, parcour_liste_from.get(index)));
					parcour_liste_suivant.add(iter_to);
					parcour_liste_dist.add(Double.valueOf(distance));
					
				}else{	
					if (new_embranchement == false && hitPoints.contains(iter_to.getRoot())){
						//System.out.println("false et pomme");
						parcour_liste_arrive.set(index, true);
						parcour_liste_suivant.set(index, iter_to);
						parcour_liste_dist.set(index,Double.valueOf(parcour_liste_dist.get(index)+distance));						
					}else {
						if (new_embranchement == false){
							//System.out.println("false et rien");
							parcour_liste_dist.set(index,Double.valueOf(parcour_liste_dist.get(index)+distance));
							parcour_liste_suivant.set(index, iter_to);						
						}else{
							//System.out.println("true et rien");
							parcour_liste_arrive.add(false);
							parcour_liste_from.add(creation_ajout(iter_from,iter_to, parcour_liste_from.get(index)));
							parcour_liste_dist.add(Double.valueOf(distance));
							parcour_liste_suivant.add(iter_to);
							parcour_liste_remove.add(iter_to);
						}
					}
				}
				new_embranchement = true;
			}
			for (Tree2D iter_remove : parcour_liste_remove)
				iter_from.getSubTrees().remove(iter_remove);
			if (new_embranchement == false){
				//System.out.println("suppression");
				parcour_liste_arrive.set(index, true);
				parcour_liste_dist.set(index, Double.MAX_VALUE);
			}
			
 			parcour_liste_remove.clear();
		}
		//if (cpt_bug >= 5)
			//return;
		//System.out.println("sortie tour");
		calculSteinerBudgetRec2(parcour_liste_suivant, parcour_liste_from, parcour_liste_dist,parcour_liste_arrive,parcouru, points, hitPoints, cpt_bug);
	}
	
	public Tree2D calculSteinerBudget(ArrayList<Point> points, int edgeThreshold, ArrayList<Point> hitPoints) {
		chemin = new ArrayList<Tree2D> ();
		Tree2D arbre_opti = calculSteiner(points, edgeThreshold, hitPoints);
		
		Tree2D maisonTree2d = getMaisonMere(arbre_opti, arbre_opti.getRoot());
		//System.out.println(maisonTree2d.getRoot());
		
		ArrayList<Double> parcour_liste_dist = new ArrayList<Double>();
		ArrayList<Boolean> parcour_liste_arrive = new ArrayList<Boolean>();
		ArrayList<ArrayList<Tree2D>> parcour_liste_from = new ArrayList<ArrayList<Tree2D>>();
		ArrayList<Tree2D> parcour_liste_suivant = new ArrayList<Tree2D>();

		parcour_liste_from.add(new ArrayList<Tree2D>());
		parcour_liste_arrive.add(false);
		parcour_liste_dist.add(Double.valueOf(0));
		parcour_liste_suivant.add(maisonTree2d);

		calculSteinerBudgetRec2(parcour_liste_suivant, parcour_liste_from, parcour_liste_dist, parcour_liste_arrive, 0, points, hitPoints, 0);
		/*int nb_points = calculSteinerBudgetRec(0, 1, maisonTree2d, null, points, hitPoints);
		chemin.add(maisonTree2d);
		System.out.println("resultat : " +nb_points);

		System.out.println("resultat : " +chemin.size());
		Tree2D steinerTree = geneTree2d(chemin.size()-1);*/

		return maisonTree2d;
	}

	public void remplirTree(Tree2D arbre, Tree2D from){
		
		ArrayList<Tree2D> fils = arbre.getSubTrees();
		
		for (Tree2D iter : fils){
			remplirTree(iter, arbre);
		}
		if (from != null){
			fils.add(from);
			arbre.setSubTrees(fils);
		}
		
	}

	public Tree2D getMaisonMere(Tree2D arbre, Point s){
		Tree2D maison_mere;

		if (arbre.getRoot() == s)
			return arbre;
		for (Tree2D iter : arbre.getSubTrees()){
			if ((maison_mere = getMaisonMere(iter, s)) != null)
				return maison_mere; 
		}
		return null;
	}

	public void test_paths_distance(){
		int[][] paths = new int[4][4];
		double[][]dist2 = new double[4][4];

		dist2[0][1] =  Double.POSITIVE_INFINITY;
		dist2[0][2] = 1;
		dist2[0][3] =  Double.POSITIVE_INFINITY;

		dist2[1][0] =  Double.POSITIVE_INFINITY;
		dist2[1][2] =  Double.POSITIVE_INFINITY;
		dist2[1][3] = 1;
		
		dist2[2][0] = 1;
		dist2[2][1] =  Double.POSITIVE_INFINITY;
		dist2[2][3] = 1;
		
		dist2[3][0] =  Double.POSITIVE_INFINITY;
		dist2[3][1] = 1;
		dist2[3][2] = 1;
		for (int i = 0; i < paths.length; i++) {
			for (int j = 0; j < paths.length; j++) {
				if (i == j) {
					dist2[i][i] = 0;
					continue;
				}
				if (dist2[i][j] == 1)
					paths[i][j] = j;
				else
					paths[i][j] = 9;
			}
		}
		for (int k = 0; k < paths.length; k++) {
			for (int i = 0; i < paths.length; i++) {
				for (int j = 0; j < paths.length; j++) {
					
					if (dist2[i][j] > dist2[i][k] + dist2[k][j]) {
						dist2[i][j] = dist2[i][k] + dist2[k][j];
						paths[i][j] = paths[i][k];
					}
						if (k == 3) {
						System.out.println("k:"+k);
						for (int i2 = 0; i2 < paths.length; i2++) {
							System.out.print(i+" : ");
							for (int j2 = 0; j2 < paths.length; j2++) {
								System.out.print(paths[i2][j2]);
							}
							System.out.println("   fin  ");
						}
						System.out.println("   ----  ");
						}
				}
			}
		}
	}
	public int[][] calculShortestPaths(ArrayList<Point> points, int edgeThreshold) {
		int[][] paths = new int[points.size()][points.size()];
		for (int i = 0; i < paths.length; i++)
			for (int j = 0; j < paths.length; j++)
				paths[i][j] = i;

		dist = new double[points.size()][points.size()];

		for (int i = 0; i < paths.length; i++) {
			for (int j = 0; j < paths.length; j++) {
				if (i == j) {
					dist[i][i] = 0;
					continue;
				}
				if (points.get(i).distance(points.get(j)) <= edgeThreshold)
					//On remplit les arretes qui sont inferieurs à edgeThreshold	
					dist[i][j] = points.get(i).distance(points.get(j));
				else
					//Si on la distance est superieur à edgeThreshold, on met la dist de cette arrete à +infini pour l'instant
					dist[i][j] = Double.POSITIVE_INFINITY;
				paths[i][j] = j;
			}
		}

		for (int k = 0; k < paths.length; k++) {
			for (int i = 0; i < paths.length; i++) {
				for (int j = 0; j < paths.length; j++) {
					if (dist[i][j] > dist[i][k] + dist[k][j]) {
						dist[i][j] = dist[i][k] + dist[k][j];
						paths[i][j] = paths[i][k];
					}
				}
			}
		}
		return paths;
	}

	//calculPathAll
	//Méthode recursive initial, elle calcule le nouvel arbre
	//Elle prend les fils du point de depart en entrée ainsi que départ pour calculer où doit passer
	//le nouvel arbre
	//Optimisation possible sur les indexOf via HashMap pour retrouver les index (indexOf en O(n))
	public ArrayList<Tree2D> calculPathAll(ArrayList<Point> points, int[][] paths, Point depart, ArrayList<Tree2D> list_arrive) {
		ArrayList<Tree2D> list_sortie = new ArrayList<Tree2D>();
		Tree2D new_elem;
		int numero_de_point;

		//On parcourt la liste des points d'arrivés qu'on devra calculer
		for (Tree2D iter : list_arrive) {
			//Récupération du prochain numéro de point à parcourir pour arriver vers le noeud arrive
			numero_de_point = paths[points.indexOf(depart)][points.indexOf(iter.getRoot())];
			if (numero_de_point != points.indexOf(iter.getRoot()))
				//Si on est pas arrivé à arrive, on continue le chemin vers arrivé en s'appelant récursivement
				//sur le point qu'on vient de trouver
				new_elem = new Tree2D(points.get(numero_de_point),
						calculPath(points, paths, points.get(numero_de_point), iter));
			else {
				//Si on est arrivé : 2 cas
				if (iter.getSubTrees() == null)
					//L'arrivé est une feuille donc on crée une feille qu'on renvoie (cas de base dans la récursion)
					new_elem = new Tree2D(points.get(numero_de_point), null);
				else
					//Si l'arrivé n'est pas une noeud, il faut rappeler calculPathAll pour calculer chaque fils;
					//Les cacluls seront Arrivé vers Fils d'Arrivé
					new_elem = new Tree2D(points.get(numero_de_point),
							calculPathAll(points, paths, iter.getRoot(), iter.getSubTrees()));
			}
			//Ajoute de l'élement new_elem dans la liste des fils du Tree2D qui a pour noeud : Point depart
			//On ajoute pour chaque élement dans list_arrive
			list_sortie.add(new_elem);
		}
		//Renvoie la liste qui contient un element au noeud Tree2D de depart qui pourra finir alors sa création
		//lors de la remontée de l'appel récursif
		return list_sortie;
	}


	//calculPath
	//Methode récursive
	//Calcul le chemin du point encours vers celui d'arrivé grace à ce qu'il y a dans paths
	//Optimisation possible sur les indexOf via HashMap pour retrouver les index (indexOf en O(n))	
	public ArrayList<Tree2D> calculPath(ArrayList<Point> points, int[][] paths, Point encours, Tree2D arrive) {
		ArrayList<Tree2D> list_sortie = new ArrayList<Tree2D>();
		Tree2D new_elem;
		int numero_de_point;

		numero_de_point = paths[points.indexOf(encours)][points.indexOf(arrive.getRoot())];
		//Récupération du prochain numéro de point à parcourir pour arriver vers le noeud arrive
		if (numero_de_point != points.indexOf(arrive.getRoot()))
			//Si on est pas arrivé à arrive, on continue le chemin vers arrivé en s'appelant récursivement
			//sur le point qu'on vient de trouver
			new_elem = new Tree2D(points.get(numero_de_point),
					calculPath(points, paths, points.get(numero_de_point), arrive));
		else {
			//Si on est arrivé : 2 cas
			if (arrive.getSubTrees() == null)
				//L'arrivé est une feuille donc on crée une feille qu'on renvoie (cas de base dans la récursion)
				new_elem = new Tree2D(points.get(numero_de_point), null);
			else
				//Si l'arrivé n'est pas une noeud, il faut rappeler calculPathAll pour calculer chaque fils;
				//Les cacluls seront Arrivé vers Fils d'Arrivé
				new_elem = new Tree2D(points.get(numero_de_point),
						calculPathAll(points, paths, arrive.getRoot(), arrive.getSubTrees()));
		}
		//Ajoute l'unique élement new_elem dans la liste des fils du Tree2D qui a pour noeud : Point encours
		list_sortie.add(new_elem);
		//Renvoie la liste qui contient un element au noeud Tree2D de encours qui pourra finir alors sa création
		//lors de la remontée de l'appel récursif
		return list_sortie;
	}

	public Tree2D calculSteiner_simple(ArrayList<Point> points, int[][] paths, ArrayList<Point> hitPoints) {
		Tree2D t = calculKruskal(hitPoints);

		Point point_root = t.getRoot();
		ArrayList<Tree2D> list_subtree = t.getSubTrees();

		return (new Tree2D(point_root, calculPathAll(points, paths, point_root, list_subtree)));
	}

	public Tree2D calculKruskal(ArrayList<Point> points) {
		// KRUSKAL ALGORITHM, NOT OPTIMAL FOR STEINER!
		ArrayList<Edge> edges = new ArrayList<Edge>();
		for (Point p : points) {
			for (Point q : points) {
				if (p.equals(q) || contains(edges, p, q))
					continue;
				edges.add(new Edge(p, q));
			}
		}
		edges = sort(edges);

		ArrayList<Edge> kruskal = new ArrayList<Edge>();
		Edge current;
		NameTag forest = new NameTag(points);
		while (edges.size() != 0) {
			current = edges.remove(0);
			if (forest.tag(current.p) != forest.tag(current.q)) {
				kruskal.add(current);
				forest.reTag(forest.tag(current.p), forest.tag(current.q));
			}
		}

		return edgesToTree(kruskal, kruskal.get(0).p);
	}

	private boolean contains(ArrayList<Edge> edges, Point p, Point q) {
		for (Edge e : edges) {
			if (e.p.equals(p) && e.q.equals(q) || e.p.equals(q) && e.q.equals(p))
				return true;
		}
		return false;
	}

	private Tree2D edgesToTree(ArrayList<Edge> edges, Point root) {
		ArrayList<Edge> remainder = new ArrayList<Edge>();
		ArrayList<Point> subTreeRoots = new ArrayList<Point>();
		Edge current;
		while (edges.size() != 0) {
			current = edges.remove(0);
			if (current.p.equals(root)) {
				subTreeRoots.add(current.q);
			} else {
				if (current.q.equals(root)) {
					subTreeRoots.add(current.p);
				} else {
					remainder.add(current);
				}
			}
		}

		ArrayList<Tree2D> subTrees = new ArrayList<Tree2D>();
		for (Point subTreeRoot : subTreeRoots)
			subTrees.add(edgesToTree((ArrayList<Edge>) remainder.clone(), subTreeRoot));

		return new Tree2D(root, subTrees);
	}

	private ArrayList<Edge> sort(ArrayList<Edge> edges) {
		if (edges.size() == 1)
			return edges;

		ArrayList<Edge> left = new ArrayList<Edge>();
		ArrayList<Edge> right = new ArrayList<Edge>();
		int n = edges.size();
		for (int i = 0; i < n / 2; i++) {
			left.add(edges.remove(0));
		}
		while (edges.size() != 0) {
			right.add(edges.remove(0));
		}
		left = sort(left);
		right = sort(right);

		ArrayList<Edge> result = new ArrayList<Edge>();
		while (left.size() != 0 || right.size() != 0) {
			if (left.size() == 0) {
				result.add(right.remove(0));
				continue;
			}
			if (right.size() == 0) {
				result.add(left.remove(0));
				continue;
			}
			if (left.get(0).distance() < right.get(0).distance())
				result.add(left.remove(0));
			else
				result.add(right.remove(0));
		}
		return result;
	}
}

class Edge {
	protected Point p, q;

	protected Edge(Point p, Point q) {
		this.p = p;
		this.q = q;
	}

	protected double distance() {
		return p.distance(q);
	}
}

class NameTag {
	private ArrayList<Point> points;
	private int[] tag;

	protected NameTag(ArrayList<Point> points) {
		this.points = (ArrayList<Point>) points.clone();
		tag = new int[points.size()];
		for (int i = 0; i < points.size(); i++)
			tag[i] = i;
	}

	protected void reTag(int j, int k) {
		for (int i = 0; i < tag.length; i++)
			if (tag[i] == j)
				tag[i] = k;
	}

	protected int tag(Point p) {
		for (int i = 0; i < points.size(); i++)
			if (p.equals(points.get(i)))
				return tag[i];
		return 0xBADC0DE;
	}

}
