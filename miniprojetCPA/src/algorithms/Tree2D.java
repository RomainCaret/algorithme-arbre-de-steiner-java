package algorithms;

import java.awt.Point;
import java.util.ArrayList;

public class Tree2D {
  private Point root;
  private ArrayList<Tree2D> subtrees;
  private Tree2D from; 

  public Tree2D (Point p, ArrayList<Tree2D> trees){
    this.root=p;
    this.subtrees=trees;
    this.from = null;
  }

  public Tree2D (Point p, ArrayList<Tree2D> trees, Tree2D from){
    this.root=p;
    this.subtrees=trees;
    this.from = from;
  }

  public Point getRoot(){
    return this.root;
  }

  public void setFrom(Tree2D from){
    this.from = from;
  }
  public void setSubTrees(ArrayList<Tree2D> trees){
    this.subtrees=trees;
  }
  public ArrayList<Tree2D> getSubTrees(){
    return this.subtrees;
  }
  public double distanceRootToSubTrees(){
    double d=0;
    for (int i=0;i<this.subtrees.size();i++) d+=subtrees.get(i).getRoot().distance(root);
    return d;
  }
  
  public double distanceTrees() {
	  double d=0;
	  if (getSubTrees() == null)
		  return 0;
	  for (int i=0;i<this.subtrees.size();i++) 
		  d+=subtrees.get(i).getRoot().distance(root) + subtrees.get(i).distanceTrees();
	  return d;
  }
}
